#Description
This image is a dev-tool for postgres 9.6. Its main purpose is to download and restore a postgres backup.
Available backup sources are :
* s3 bucket
* http server

#Build:
docker build -t arxcf/artifact-postgres .

#Push
docker push arxcf/artifact-postgres

#Run with auto download last artifact of the s3 bucket:
docker run `
-e POSTGRES_DB=crm `
-e POSTGRES_USER=postgres `
-e POSTGRES_PASSWORD=manager `
-e OS_AUTH_URL="https://auth.cloud.ovh.net/v3/" `
-e OS_USERNAME=Am6HRsRsWzuM `
-e OS_PASSWORD=NBknsZTNc76T23pZqdWGnFtYpjmewhbq `
-e OS_TENANT_ID="e2191572a0094779a812fb3e37ab846d" `
-e S3_ENDPOINT=https://s3.sbg.cloud.ovh.net `
-e S3_REGION=sbg `
-e S3_BUCKET="s3://dev-backup" `
-e S3_FILE_FILTER="crm-db-arx" `
--name crm-db-arx --rm arxcf/artifact-postgres

#Run with specific artifact from s3:
docker run `
-e POSTGRES_DB=crm `
-e POSTGRES_USER=postgres `
-e POSTGRES_PASSWORD=manager `
-e OS_AUTH_URL="https://auth.cloud.ovh.net/v3/" `
-e OS_USERNAME=Am6HRsRsWzuM `
-e OS_PASSWORD=NBknsZTNc76T23pZqdWGnFtYpjmewhbq `
-e OS_TENANT_ID="e2191572a0094779a812fb3e37ab846d" `
-e S3_ENDPOINT=https://s3.sbg.cloud.ovh.net `
-e S3_REGION=sbg `
-e S3_ARTIFACT="s3://feature-dump/crm-db-arx.tar.gz" `
--name crm-db-arx --rm arxcf/artifact-postgres

#Run with download specific artifact from any http server
docker run `
-e POSTGRES_DB=crm `
-e POSTGRES_USER=postgres `
-e POSTGRES_PASSWORD=manager `
-e ARTIFACT_URL=http://nexus.arxcf.com/repository/raw-snapshots/com/arx/crm-bdd/arx-SNAPSHOT/crm-bdd-arx-20211012.151230.dump `
-e ARTIFACT_USERNAME=admin `
-e ARTIFACT_PASSWORD=Arx12345 `
--name crm-db-arx --rm arxcf/artifact-postgres