#!/bin/bash

#Init environment
DUMP_DIR=/tmp/dumps
DUMP_PATH_ARCHIVE=$DUMP_DIR/$POSTGRES_DB.tar.gz
BUCKET_LS=/tmp/bucketLs

#find s3 artifact
if [[ ! -z "$S3_ARTIFACT" ]] || [[ ! -z "$S3_BUCKET" ]]; then
    
    #get the s3 credentials if needed
    if [[ -z "$S3_ACCESS_KEY_ID" ]] && [[ ! -z "$OS_AUTH_URL" ]]; then
        /usr/local/bin/aws-cred.sh
    else
        envsubst < "/var/lib/postgresql/.aws/config.template" > "/var/lib/postgresql/.aws/config"
    fi
    
    cat /var/lib/postgresql/.aws/config

    #find last artifact if not provided
    if [[ -z "$S3_ARTIFACT" ]] && [[ ! -z "$S3_BUCKET" ]]; then
        echo "Artifact not provided searching for most recent one"
        if [[ ! -z "$S3_FILE_FILTER" ]]; then
            echo "s3 startWith filter provided: $S3_FILE_FILTER"
            aws --profile default s3 ls $S3_BUCKET/$S3_FILE_FILTER > $BUCKET_LS
            echo "Bucket $S3_BUCKET filtered content:"
        else 
            echo "no filter provided"
            aws --profile default s3 ls $S3_BUCKET > $BUCKET_LS
            echo "Bucket $S3_BUCKET unfiltered content:"
        fi
        cat $BUCKET_LS
        export S3_ARTIFACT="$S3_BUCKET/"$(cat $BUCKET_LS | awk -v NF=4 '{for(i=5;i<=NF;i++) $4=$4 OFS $i; print $4}' | sort | tail -n 1)
    fi

    #download s3 artifact
    if [[ ! -z "$S3_ARTIFACT" ]]; then
        echo "downloading s3 artifact $S3_ARTIFACT"
        aws --profile default s3 cp "$S3_ARTIFACT" $DUMP_PATH_ARCHIVE
    else
        echo "No artifact found from s3"
    fi

#download from any http server with or without crednetials
elif [[ ! -z "$ARTIFACT_URL" ]]; then
    if [[ ! -z "$ARTIFACT_USERNAME" ]] && [[ ! -z "$ARTIFACT_PASSWORD" ]]; then
        curl -u $ARTIFACT_USERNAME:$ARTIFACT_PASSWORD $ARTIFACT_URL --output $DUMP_PATH_ARCHIVE
    else
        curl $ARTIFACT_URL --output $DUMP_PATH_ARCHIVE
    fi
fi

#extract and load dumps if present
echo "dump dir content:"
ls -lrth $DUMP_DIR
for DUMP_FILE in $(ls $DUMP_DIR); do
    DB=$(echo $DUMP_FILE | cut -d . -f 1);
    DUMP_PATH_ARCHIVE=$DUMP_DIR/$DUMP_FILE
    echo "handling file $DUMP_PATH_ARCHIVE"
    gunzip -c $DUMP_PATH_ARCHIVE | tar -t > /dev/null
    if [ $? == 0 ]; then
        echo "file is a valid tar.gz";
        DUMP_PATH_EXTRACT=/tmp/$DB;
        rm -rf $DUMP_PATH_EXTRACT
        mkdir -p $DUMP_PATH_EXTRACT;
        tar xzvf $DUMP_PATH_ARCHIVE -C $DUMP_PATH_EXTRACT;
        DUMP_PATH=$(ls $DUMP_PATH_EXTRACT/*.dump | head -n 1)
        #can't delete a mounted file
        rm -rf $DUMP_PATH_ARCHIVE || true;
    else
        echo "file is NOT a valid tar.gz, will try to load it as is";
        DUMP_PATH=$DUMP_PATH_ARCHIVE
    fi

    psql -c "CREATE DATABASE $DB;" || true
    echo 'Restoring dump'
    pg_restore -d ${DB} -U ${POSTGRES_USER} -j8 --no-owner --no-acl --disable-triggers -Fc $DUMP_PATH;

done

echo 'Allowing everyone to access postgres server'
sed -i -e "s/^#listen_addresses =.*\$/listen_addresses = '*'/" $PGDATA/postgresql.conf

echo 'Removing md5 authentication'
sed -i -e "/host all all all md5/d" /var/lib/postgresql/data/pg_hba.conf

echo 'Adding trust authentication'
echo "host    all    all    all    trust" >> $PGDATA/pg_hba.conf

echo 'Initialization complete'