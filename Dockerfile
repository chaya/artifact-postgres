FROM postgres:9.6-alpine
LABEL MAINTENER="Arx CF"

WORKDIR /home

RUN apk add --update \
    bash \
    curl \
    jq \
    gettext \
    python3 \
    py3-pip \
    && pip3 install --upgrade pip \
    && pip3 install --no-cache-dir \
        awscli \
        awscli-plugin-endpoint \
    && rm -rf /var/cache/apk/*

COPY aws-config /var/lib/postgresql/.aws/config.template
COPY aws-config /var/lib/postgresql/.aws/config
COPY run.sh /docker-entrypoint-initdb.d
RUN chmod +x /docker-entrypoint-initdb.d/run.sh
COPY aws-cred.sh /usr/local/bin/aws-cred.sh
RUN chmod +x /usr/local/bin/aws-cred.sh
RUN chmod 777 /var/lib/postgresql/.aws/config
RUN chmod 777 /tmp
RUN chmod 777 /var/lib/postgresql/.aws/config