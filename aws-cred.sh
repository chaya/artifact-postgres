echo "Generating s3 access key and secret"
set +x
TMP_FILE=$(mktemp)
TMP_S3_FILE=$(mktemp)
OS_USER_ID=$(curl -s -D $TMP_FILE -X POST "${OS_AUTH_URL}auth/tokens" -H "Content-Type: application/json" -d '{"auth":{"identity":{"methods":["password"],"password":{"user":{"name":"'$OS_USERNAME'","domain":{"id":"default"},"password":"'$OS_PASSWORD'"}}},"scope":{"project":{ "id":"'$OS_TENANT_ID'","domain":{"id":"default"}}}}}' | jq -r '.["token"]["user"]["id"]')
OS_TOKEN=$(awk '/^X-Subject-Token/ {print $2}' $TMP_FILE |  tr -d "\r")
curl -s -X POST -H "Content-Type: application/json" -H "X-Auth-Token: $OS_TOKEN" -d '{"tenant_id": "'$OS_TENANT_ID'"}' "${OS_AUTH_URL}users/${OS_USER_ID}/credentials/OS-EC2" > $TMP_S3_FILE

export S3_ACCESS_KEY_ID=`cat $TMP_S3_FILE | jq -r '.["credential"]["access"]'`
export S3_SECRET_ACCESS_KEY=`cat $TMP_S3_FILE | jq -r '.["credential"]["secret"]'`

envsubst < "/var/lib/postgresql/.aws/config.template" > "/var/lib/postgresql/.aws/config"

set -x